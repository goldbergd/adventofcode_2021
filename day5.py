import puzzle


class day5(puzzle.Puzzle):
    class Board(object):
        def __init__(self) -> None:
            super().__init__()
            self.board = {}

        def add_point(self, point) -> None:
            if point not in self.board:
                self.board[point] = 0
            self.board[point] += 1

        def add_line(self, point0, point1) -> None:
            # print(f"Adding line: {point0} - {point1}")
            if point0[0] != point1[0]:
                slope = float(point1[1] - point0[1]) / float(point1[0] - point0[0])
                for x in range(min(point0[0], point1[0]), max(point0[0], point1[0])+1):
                    self.add_point((x, int(round((x-point0[0]) * slope + point0[1]))))
            else:
                for y in range(min(point0[1], point1[1]), max(point1[1], point0[1]) + 1):
                    self.add_point((point0[0], y))

        def count_points(self, key=lambda k, v: v) -> int:
            return sum([key(x, self.board[x]) for x in self.board])

        def __str__(self) -> str:
            return str(self.board)

    def __init__(self) -> None:
        super().__init__(5)
        self.board = day5.Board()
        self.is_part2 = True

    def process_input_line(self, line) -> None:
        points = line.split(" -> ")
        p0 = tuple(int(x.strip()) for x in points[0].split(','))
        p1 = tuple(int(x.strip()) for x in points[1].split(','))
        if p0[0] == p1[0] or p0[1] == p1[1] or self.is_part2:
            self.board.add_line(p0, p1)

    def finished(self) -> None:
        # print(self.board)
        print(self.board.count_points(lambda k, v: v > 1))


if __name__ == "__main__":
    d = day5()
    d.run()
    test = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""
    # d.test_run(test.split('\n'))
