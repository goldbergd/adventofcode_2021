import puzzle


class day3(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(3)
        self.count = 0
        self.bit_counts = None
        self.is_part2 = True
        self.all = set()

    def process_input_line(self, line) -> None:
        if self.bit_counts is None and len(line):
            self.bit_counts = [0] * len(line)
        if self.is_part2:
            self.process_input_line2(line)
        else:
            self.process_input_line1(line)

    def finished(self) -> None:
        if self.is_part2:
            self.finished2()
        else:
            self.finished1()

    def process_input_line1(self, line) -> None:
        if len(self.bit_counts) != len(line):
            raise Exception(f"Index {self.count}, Length needs to be {len(line)}")

        for i in range(len(line)):
            self.bit_counts[i] += int(line[i])
        self.count += 1

    def finished1(self) -> None:
        gamma_str = ""
        epsilon_str = ""
        for count in self.bit_counts:
            if count > self.count/2:
                gamma_str += '1'
                epsilon_str += '0'
            else:
                gamma_str += '0'
                epsilon_str += '1'
        gamma = int(gamma_str, base=2)
        epsilon = int(epsilon_str, base=2)
        print(gamma*epsilon)

    def process_input_line2(self, line) -> None:
        self.all.add(line)

    def finished2(self) -> None:
        oxygen_set = self.all.copy()
        for i in range(len(self.bit_counts)):
            count_oxygen = sum([int(v[i]) for v in oxygen_set])
            if count_oxygen >= len(oxygen_set)/2:
                oxygen_set = set(x for x in oxygen_set if x[i] == '1')
            else:
                oxygen_set = set(x for x in oxygen_set if x[i] == '0')
            if len(oxygen_set) == 1:
                break

        co2_set = self.all.copy()
        for i in range(len(self.bit_counts)):
            count_co2 = sum([int(v[i]) for v in co2_set])
            if count_co2 >= len(co2_set)/2:
                co2_set = set(x for x in co2_set if x[i] == '0')
            else:
                co2_set = set(x for x in co2_set if x[i] == '1')
            if len(co2_set) == 1:
                break
        print(int(oxygen_set.pop(), base=2) * int(co2_set.pop(), base=2))


if __name__ == "__main__":
    d = day3()
    d.run()
    test = """00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"""
    # d.test_run(test.split('\n'))
