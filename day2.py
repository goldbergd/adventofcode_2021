import puzzle


class day2(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(2)
        self.horizontal = 0
        self.depth = 0
        self.aim = 0
        self.is_part2 = True

    def process_input_line(self, line) -> None:
        input = line.split(" ")
        if len(input) != 2:
            return
        if self.is_part2:
            if "forward" in input[0]:
                self.horizontal += int(input[1])
                self.depth = max(self.depth + int(input[1])*self.aim, 0)
            elif "down" in input[0]:
                self.aim += int(input[1])
            elif "up" in input[0]:
                self.aim -= int(input[1])
        else:
            if "forward" in input[0]:
                self.horizontal += int(input[1])
            elif "down" in input[0]:
                self.depth += int(input[1])
            elif "up" in input[0]:
                self.depth = max(self.depth - int(input[1]), 0)

    def finished(self) -> None:
        print(self.horizontal * self.depth)


if __name__ == "__main__":
    d = day2()
    d.run()
    test = """forward 5
down 5
forward 8
up 3
down 8
forward 2"""
    # d.test_run(test.split('\n'))
