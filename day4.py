import puzzle
from collections import deque


class BingoCard(object):
    def __init__(self) -> None:
        super().__init__()
        self.board = []
        self.width = 0
        self.height = 0
        self.called_numbers = deque()

    def add_row(self, num) -> None:
        row = [int(x) for x in num.split()]
        if self.width == 0:
            self.width = len(row)
        if len(row) != self.width:
            raise Exception(f"Row doesn't match width. {self.height}: {len(row)} width: {self.width}")
        self.board.extend(row)
        self.height += 1

    def call(self, num) -> None:
        self.called_numbers.append(num)

    def has_win(self) -> bool:
        # Horizontal
        for j in range(self.height):
            row_start = j*self.height
            row_stop = row_start + self.width
            if all(x in self.called_numbers for x in self.board[row_start:row_stop]):
                # print("horizontal win")
                # print(self.board[row_start:row_stop])
                return True
        # Vertical
        for i in range(self.width):
            if all(x in self.called_numbers for x in self.board[i::self.width]):
                # print("vert win")
                # print(self.board[i::self.width])
                return True
        return False

    def score(self) -> int:
        # sum of all unmarked numbers * last number that was called
        return sum(x for x in self.board if x not in self.called_numbers) * self.called_numbers[-1]


class day4(puzzle.Puzzle):

    def __init__(self) -> None:
        super().__init__(4)
        self.called_numbers = []
        self.boards = deque()
        self.is_part2 = True

    def process_input_line(self, line) -> None:
        if len(line):
            if not self.called_numbers:
                self.called_numbers = [int(x) for x in line.split(',')]
            else:
                self.boards[-1].add_row(line)
        else:
            self.boards.append(BingoCard())

    def finished(self) -> None:
        boards_won = [False] * len(self.boards)
        for num in self.called_numbers:
            for i in range(len(self.boards)):
                if not boards_won[i]:
                    self.boards[i].call(num)
                    if self.boards[i].has_win():
                        if self.is_part2:
                            if sum(not x for x in boards_won) == 1:
                                print(self.boards[i].score())
                                return
                            else:
                                boards_won[i] = True
                        else:
                            print(self.boards[i].score())
                            return


if __name__ == "__main__":
    d = day4()
    d.run()
    test = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"""
    # d.test_run(test.split('\n'))
