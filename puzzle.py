import urllib.request
import os


class Puzzle(object):
    def __init__(self, day) -> None:
        super().__init__()
        self.day = day
        self.filename = os.path.join("inputs", f"day{self.day}.txt")

    def process_input_line(self, line) -> None:
        pass

    def finished(self) -> None:
        pass

    def run(self):
        if not os.path.isfile(self.filename):
            print(f"https://adventofcode.com/2021/day/{self.day}/input")
            urllib.request.urlretrieve(f"https://adventofcode.com/2021/day/{self.day}/input", self.filename)
        with open(self.filename) as file:
            for line in file:
                self.process_input_line(line.rstrip())
        self.finished()

    def test_run(self, list_inputs):
        for line in list_inputs:
            self.process_input_line(line.rstrip())
        self.finished()
