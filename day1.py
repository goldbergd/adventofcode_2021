import puzzle
from collections import deque


class day1(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(1)
        self.count = 0
        self.previous = None
        self.is_part2 = True
        self.prior_sums = deque([None] * 3)

    def process_input_line(self, line) -> None:
        cur = int(line)
        # Part 1
        if self.previous is not None:
            if not self.is_part2 and cur > self.previous:
                self.count += 1
        self.previous = cur

        # Part 2
        last_sum = self.prior_sums[-1]
        new_sums = deque([x + cur if x is not None else None for x in self.prior_sums])
        new_sums.appendleft(cur)
        new_sums.pop()
        if self.is_part2 and last_sum is not None and new_sums[-1] > last_sum:
            self.count += 1
        self.prior_sums = new_sums

    def finished(self) -> None:
        print(self.count)


if __name__ == "__main__":
    d = day1()
    d.run()
    test = """199
200
208
210
200
207
240
269
260
263"""
    # d.test_run(test.split('\n'))
