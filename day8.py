import puzzle
from collections import deque
#   0:      1:      2:      3:      4:
#  aaaa    ....    aaaa    aaaa    ....
# b    c  .    c  .    c  .    c  b    c
# b    c  .    c  .    c  .    c  b    c
#  ....    ....    dddd    dddd    dddd
# e    f  .    f  e    .  .    f  .    f
# e    f  .    f  e    .  .    f  .    f
#  gggg    ....    gggg    gggg    ....

#   5:      6:      7:      8:      9:
#  aaaa    aaaa    aaaa    aaaa    aaaa
# b    .  b    .  .    c  b    c  b    c
# b    .  b    .  .    c  b    c  b    c
#  dddd    dddd    ....    dddd    dddd
# .    f  e    f  .    f  e    f  .    f
# .    f  e    f  .    f  e    f  .    f
#  gggg    gggg    ....    gggg    gggg


def decode(word, a, b, c, d, e, f, g) -> int:
    NUMBERS = ['abcefg',
               'cf',
               'acdeg',
               'acdfg',
               'bcdf',
               'abdfg',
               'abdefg',
               'acf',
               'abcdefg',
               'abcdfg']
    lut = {'a': a,
           'b': b,
           'c': c,
           'd': d,
           'e': e,
           'f': f,
           'g': g}
    out = -1
    for i in range(len(NUMBERS)):
        coded_num = ''.join(sorted(''.join([lut[x] for x in NUMBERS[i]])))
        if coded_num == word:
            return i
    return out


def diff(str1, str2) -> str:
    # What str1 has that str2 doesn't
    out = []
    for s in str1:
        if s not in str2:
            out.append(s)
    return ''.join(out)


def same(str1, str2) -> str:
    # What str1 has that str2 does also
    out = []
    for s in str1:
        if s in str2:
            out.append(s)
    return ''.join(out)


class day8(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(8)
        self.is_part2 = True
        self.count = 0

    def process_input_line(self, line) -> None:
        inputs = line.split('|')[0].strip().split()
        inputs_sorted = {}
        for i in range(len(inputs)):
            l = len(inputs[i])
            if l not in inputs_sorted:
                inputs_sorted[l] = []
            inputs_sorted[l].append(''.join(sorted(inputs[i])))
        one = inputs_sorted[2][0]
        four = inputs_sorted[4][0]
        seven = inputs_sorted[3][0]
        eight = inputs_sorted[7][0]

        # Find 3
        for i in inputs_sorted[5]:
            # Only 3 will have 1 inside of it
            if len(same(i, one)) == 2:
                three = i
                break
        # 7 - 1 (acf - cf)
        a = diff(seven, one)
        # 4 - 3 (bcdf - acdfg)
        b = diff(four, three)
        # c = LATER DOWN
        # 4 - 1 same as 3
        d = same(diff(four, one), three)
        # (8 - 9 - 4) + (8 - 6 - 4) + (8 - 0 - 4)
        e = ''.join(diff(diff(eight, six_len_word), four) for six_len_word in inputs_sorted[6])
        # 5 - (3-1) - b
        # f = LATER DOWN
        # 3 - 4 - 7
        g = diff(diff(three, four), seven)

        # Find 2
        for i in inputs_sorted[5]:
            if e in i:
                two = i
                break
        # 2 - a - d - e - g
        c = two.replace(a, '').replace(d, '').replace(e, '').replace(g, '')
        # 1 - c
        f = one.replace(c, '')

        outputs = line.split('|')[1].strip().split()
        nums = deque()
        for out in outputs:
            num = decode(''.join(sorted(out)), a, b, c, d, e, f, g)
            nums.appendleft(num)
        # print(nums)
        if self.is_part2:
            self.count += sum(nums[i] * pow(10, i) for i in range(len(nums)))
        else:
            self.count += 1 if num == 1 or num == 4 or num == 7 or num == 8 else 0

    def finished(self) -> None:
        print(self.count)


if __name__ == "__main__":
    d = day8()
    d.run()
    # test = """acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"""
    test = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |fgae cfgab fg bagce"""
    # d.test_run(test.split('\n'))
