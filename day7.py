import puzzle


class day7(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(7)
        self.crabs = []
        self.is_part2 = True

    def process_input_line(self, line) -> None:
        self.crabs = [int(f) for f in line.split(',')]

    def finished(self) -> None:
        max_pos = max(self.crabs)
        possible_pos = [-1] * (max_pos + 1)
        lut = {x: sum(range(x+1)) for x in range(max_pos+1)}
        for pos in range(max_pos + 1):
            possible_pos[pos] = sum([lut[abs(c - pos)] if self.is_part2 else abs(c - pos) for c in self.crabs])
        print(min(possible_pos))


if __name__ == "__main__":
    d = day7()
    d.run()
    test = """16,1,2,0,4,2,7,1,2,14"""
    # d.test_run(test.split('\n'))
