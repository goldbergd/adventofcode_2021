import puzzle
from collections import deque


class day6(puzzle.Puzzle):
    def __init__(self) -> None:
        super().__init__(6)
        self.fish_ages = [0] * 9
        self.is_part2 = True

    def process_input_line(self, line) -> None:
        for f in line.split(','):
            self.fish_ages[int(f)] += 1

    def finished(self) -> None:
        for day in range(256 if self.is_part2 else 80):
            prev = 0
            for age in range(len(self.fish_ages) - 1, -1, -1):
                t = self.fish_ages[age]
                self.fish_ages[age] = prev
                prev = t
            self.fish_ages[6] += prev
            self.fish_ages[len(self.fish_ages) - 1] += prev
        print(sum(self.fish_ages))


if __name__ == "__main__":
    d = day6()
    d.run()
    test = """3,4,3,1,2"""
    # d.test_run(test.split('\n'))
